use clap::Parser;
extern crate rayon;

mod io;
mod preprocess;
mod train;


/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the .txt data files
    #[clap(short, long)]
    data_path: String,

    /// How many epochs you train for
    #[clap(short, long, default_value_t = 5)]
    epochs: u8,

    /// Learning rate of the SGD algorithm
    #[clap(short, long, default_value_t = 0.025)]
    learning_rate: f32,

    /// Number of negative samples
    #[clap(short, long, default_value_t = 5)]
    negative_samples: u8,

    /// Size of the context window
    #[clap(short, long, default_value_t = 5)]
    window_size: u64,

    /// Embedding dimensionality samples
    #[clap(long, default_value_t = 96)]
    dimensionality: u64,

}

fn main() {
    let args = Args::parse();
    let dataset_name: String = args.data_path;
    let data = preprocess::generate_data(dataset_name);
    let data_len = data.len() as u64;
    let vocab_size = 200000;

    let (word_vectors, context_vectors) = train::train_embedding(data, data_len, vocab_size, args.dimensionality, args.epochs, args.learning_rate, args.negative_samples, args.window_size);

    let _ = io::write_embeddings(word_vectors, context_vectors);

}
